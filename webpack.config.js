const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

//const sourcePath = path.join(__dirname, './client');
const staticsPath = path.join(__dirname, './dist');


module.exports = function (env) {
    const nodeEnv = env && env.prod ? 'production' : 'development';
    const isProd = nodeEnv === 'production';
    const extractGlobal = new ExtractTextPlugin('global.css');
    const plugins = [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
          //  minChunks: Infinity,
            filename: 'vendor.bundle.js'
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: nodeEnv,
        }),
        extractGlobal
    ];

    if (isProd) {
        plugins.push(
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    screw_ie8: true,
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true,
                },
                output: {
                    comments: false,
                },
            })
        );
    }


    return {
        //devtool: isProd ? 'source-map' : 'eval',
        devtool: 'source-map',

        entry: {
            westa: [
                'webpack-dev-server/client?http://localhost:8080',
                //  'webpack/hot/only-dev-server',
                './js/client.js',
            ],
            vendor: ['react', 'react-dom', 'react-router', 'react-redux', 'redux', 'classnames', 'axios', 'reselect',
                'react-router-redux', 'redux-thunk', 'redux-form', 'ramda', 'redux-devtools-extension']
        },
        output: {
            path: staticsPath,
            filename: '[name].bundle.js',
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'file-loader',
                        query: {
                            name: '[name].[ext]'
                        },
                    },
                },
                {
                    test: /\.(js|jsx)$/,
                    use: [
                        //'react-hot-loader',
                        'babel-loader'
                    ],
                    exclude: /node_modules/
                },
                {
                    test: /\.scss$/,
                    use: [
                        {loader: 'style-loader'},
                        {
                            loader: 'css-loader?sourceMap&modules&localIdentName=[local]___[hash:base64:5]&importLoaders=1',

                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        },
                        {loader: 'sass-loader'}
                    ],
                    exclude: /global.scss/
                },
                {
                    test: /global.scss/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'sass-loader'
                    ]
                    // use: extractGlobal.extract([
                    //     'style-loader',
                    //     'css-loader',
                    //     'sass-loader'
                    // ])
                }
            ]
        },
        plugins,
        performance: isProd && {
            maxAssetSize: 100,
            maxEntrypointSize: 300,
            hints: 'warning',
        },

        stats: {
            verbose: true,
            colors: {
                green: '\u001b[32m',
            }
        },
    }

}