import React from 'react';
import styles from './app-loading-indicator.scss';

export const AppLoadingIndicator = () =>
<div className={styles.bowlG}>
    <div className={styles.bowl_ringG}>
        <div className={styles.ball_holderG}>
            <div className={styles.ballG}>
            </div>
        </div>
    </div>
</div>
