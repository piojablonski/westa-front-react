import React from 'react';
import {appConfig} from "../../appConfig";

export const UmbracoImage = props => {
    const {className, umbracoImage, imageQuality} = props;

    if (!umbracoImage || !umbracoImage.url) {
        console.warn("umbraco image not found");
        return;
    }

    const bcgImageValue = `url(${appConfig.backendUrl}${umbracoImage.url}?quality=${imageQuality})`;

    let elemStyle = {
        backgroundImage: bcgImageValue
    };

    if (umbracoImage.umbracoFile.focalPoint != null) {
        let positionLeft = Math.round(umbracoImage.umbracoFile.focalPoint.left * 100);
        let positionTop = Math.round(umbracoImage.umbracoFile.focalPoint.top * 100);
        elemStyle.backgroundPosition = `${positionLeft}% ${positionTop}%`;
    }

    console.log(bcgImageValue);

    return (
        <span className={className} style={elemStyle}></span>
    )
}

UmbracoImage.propTypes = {
    umbracoImage: React.PropTypes.object.isRequired,
    imageQuality: React.PropTypes.number.isRequired
}

UmbracoImage.defaultProps = {
    imageQuality: 80
}