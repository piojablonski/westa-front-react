import React from 'react';
import {Page} from "../page/page";
import {UmbracoImage} from '../shared/umbraco-image';
import classNames from 'classnames';
import R from 'ramda';

const mappedIndex = R.addIndex(R.map);

export const GridRenderer = ({grid}) => {

    console.log(grid);
    const res = mappedIndex((s, idx) => <GridSection key={idx} section={s}/>, grid.sections);

    return <div id="grid">{res}</div>
}

GridRenderer.propTypes = {
    grid: React.PropTypes.any.isRequired
}

function GridSection({section}) {
    const rows = mappedIndex((r, i) => <GridRow key={i} row={r}/>, section.rows);
    return (<div>{rows}</div>);
}


function GridRow({row}) {
    const areas = mappedIndex((area, idx) => <GridArea area={area} key={idx}/>, row.areas)
    return (<div>{areas}</div>)
}

function GridArea({area}) {
    const controls = mappedIndex((control, idx) => <GridControl key={idx} control={control}/>, area.controls);
    return <div>{controls}</div>
}

function GridControl({control}) {

    switch (control.editor.alias) {
        case 'pt-headline-1':
            return <p className="text-h1">{control.value}</p>;
        case 'pt-headline-2':
            return <p className="text-h2 text-wrapper-h2">{control.value}</p>;
        case 'pt-headline-3':
            return  <p className="text-h3 text-wrapper-h3">{control.value}</p>;
        case 'pt-paragraph':
            return <p className="text-p text-wrapper-p">{control.value}</p>;
        case 'w-headline-zakres-prac':
            return <h3 className="ws-description__header">{control.value}</h3>;
        case 'w-rte-zakres-prac' :
            return <div dangerouslySetInnerHTML={{__html:control.value}}></div>
        default:
            return null;
    }






}


