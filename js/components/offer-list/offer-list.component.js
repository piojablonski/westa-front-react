import React from 'react';
import {Link} from 'react-router';
import style from './offer-list.component.scss';
import classNames from 'classnames';
import R from 'ramda';
import {Page} from "../page/page";
import * as stringUtils from '../../stringUtils';

@Page()
export class OfferListComponent extends React.Component {
    render() {
        const {page} = this.props;
        return (
            <div className={style.container}>
                <div className={classNames(style.projects_list)}>
                    {R.addIndex(R.map)((prj, idx) => <Project projekt={prj} key={idx}/>, page.children)}
                </div>
            </div>

        );
    }
}

const Project = ({projekt}) =>
        <Link to={projekt.url} className={classNames(style.projects_list_item)}>
            <img className={classNames(style.projects_list_image_f)}
                 src={stringUtils.backendImageUrl(projekt.naglowek.url, { quality:80, width: 640})}/>
            <div className={classNames(style.projects_list_info_block)}>
                <span className={classNames(style.projects_list_triangle)}></span>
                <div className={classNames(style.projects_list_info_name)}>
                    <span className={classNames(style.projects_list_accent_line, 'accent-line')}></span>
                    <span className={classNames(style.projects_list_label_name)}>{projekt.title}</span>
                </div>
                <div className={classNames(style.projects_list_divider)}></div>
                <div className={classNames(style.projects_list_info_surface)}>
                    <span className={classNames(style.projects_list_label)}>POWIERZCHNIA UŻYTKOWA:</span>
                    <span
                        className={classNames(style.projects_list_value)}>{stringUtils.surfaceFormat(projekt.powierzchnia)}
                        m<sup>2</sup></span></div>
                <div className={classNames(style.projects_list_info_price)}>
                    <span className={classNames(style.projects_list_label)}>CENA DOMU:</span>
                    <span
                        className={classNames(style.projects_list_value)}>{stringUtils.currencyZl(projekt.cenaDomu)}</span>
                </div>
            </div>
        </Link>
