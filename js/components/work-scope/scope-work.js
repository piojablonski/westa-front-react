import React from 'react';
import {Page} from "../page/page";
import {UmbracoImage} from '../shared/umbraco-image';
import {GridRenderer} from '../shared/grid-renderer';
import classNames from 'classnames';
import style from './scope-work.scss';

@Page('zakresPrac')
export class ScopeOfWorkComponent extends React.Component {

    render() {
        const {page} = this.props;
        console.log(style);
        return (
            <div className={classNames(style.container)}>
                <UmbracoImage className={classNames('bcg-image', style.ws_image)} umbracoImage={page.obrazekNaglowek}/>

                <div className={classNames(style.ws_step_1_container)}>
                    <span className={classNames(style.accent_line, style.ws_accent_line)}></span>
                    <h2 className={classNames(style.ws_header)}>{page.step1Header}</h2>
                    <h3 className={classNames(style.ws_subheader)}>{page.step1Subheader}</h3>
                    <div className={classNames(style.ws_box_container)}>
                        <div className={classNames(style.ws_box)}>
                            <svg className={classNames(style.ws_box_icon)}>
                                <use xlinkHref="/assets/icons/westa-icons.svg#westa-zakres-prac-dom"></use>
                            </svg>
                            <span className={classNames(style.ws_box_title)}>{page.step1motto1}</span>
                            <span className={classNames(style.ws_box_bottom_accent)}></span>
                        </div>
                        <div className={classNames(style.ws_box)}>
                            <svg className={classNames(style.ws_box_icon)}>
                                <use xlinkHref="/assets/icons/westa-icons.svg#westa-zakres-prac-dlugopis"></use>
                            </svg>
                            <span className={classNames(style.ws_box_title)}>{page.step1motto2}</span>
                            <span className={classNames(style.ws_box_bottom_accent)}></span>
                        </div>
                        <div className={classNames(style.ws_box)}>
                            <svg className={classNames(style.ws_box_icon)}>
                                <use xlinkHref="/assets/icons/westa-icons.svg#westa-zakres-prac-lupa"></use>
                            </svg>
                            <span
                                className={classNames(style.ws_box_title)}>{page.step1motto3}</span>
                            <span className={classNames(style.ws_box_bottom_accent)}></span>
                        </div>
                    </div>
                </div>

                <div className={classNames(style.ws_step_2_container)}>
                    <span className={classNames(style.accent_line, style.ws_accent_line)}></span>
                    <h2 className={classNames(style.ws_header)}>{page.step2Header}</h2>
                    <h3 className={classNames(style.ws_subheader)}>{page.step2Subheader}</h3>
                    <span className={classNames(style.ws_intro)}>{page.step2Intro}</span>
                    <div className={classNames(style.ws_description, 'ws-description')}>
                        <GridRenderer grid={page.blog} />
                    </div>
                </div>
            </div>
        )
    }
}
