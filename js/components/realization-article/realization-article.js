import React from 'react';
import {Page} from "../page/page";
import styles from '../offer-article/offer-article.scss';
import classNames from 'classnames';
import {UmbracoImage} from '../shared/umbraco-image';
import {Link} from 'react-router';
import * as stringUtils from "../../stringUtils";
import {DocumentLinks, SwiperVisualisations} from '../offer-article/offer-article';

const Details = ({content}) => (
    <div className={classNames(styles.projekt_details_block)}>
        <div className={classNames(styles.projekt_description)}>
            <div className={classNames(styles.project_field_name)}>{content.title}</div>
            <div className={classNames(styles.project_field_divider)}></div>
            <div className={classNames(styles.project_field, styles.project_field_with_margin)}>
                <span className={classNames(styles.project_field_title)}>POWIERZCHNIA UŻYTKOWA:</span>&nbsp;<span
                className={classNames(styles.project_field_data)}>{stringUtils.surfaceFormat(content.powierzchnia)}
                m<sup>2</sup></span>
            </div>
            <div className={classNames(styles.project_field)}>
                <span className={classNames(styles.project_field_title)}>STATUS:</span>&nbsp;<span
                className={classNames(styles.project_field_data)}>{content.status}</span>
            </div>
            <div className={classNames(styles.project_field_description)}>{content.opis}</div>
        </div>

    </div>
)

@Page()
export class RealizationArticleComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {page, homePage} = this.props;
        return (
            <div className={classNames(styles.projekt_container)}>
                <div className={classNames(styles.prj_header)}>
                    <UmbracoImage className={classNames('bcg-image', styles.prj_header_image)}
                                  umbracoImage={page.naglowek}/>

                </div>
                <Details content={page}/>
                <DocumentLinks content={page}/>
                {/*<FloorPlan content={page} plan={page.planParteru}/>*/}
                {/*<FloorPlan content={page} plan={page.planPietra}/>*/}
                <SwiperVisualisations content={page}/>

            </div>
        );
    }
}