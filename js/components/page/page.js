import React from 'react';
import classNames from 'classnames';
import style from './page.scss';
import {connect} from "react-redux";
import {pageDataSelector, getUmbracoNodeByTypeAlias} from "../../selectors/umbraco-node.selectors";
import {FooterComponent} from '../';

export const Page = (documentTypeAlias) => WrappedComponent => {
    const mapStateToProps = state => ({
        page: pageDataSelector(state, documentTypeAlias),
        homePage : getUmbracoNodeByTypeAlias(state, 'homePage')
    });

    const PageComponent = class extends React.Component {
        componentDidMount() {
            console.log('creating page', documentTypeAlias)
        }

        render() {
            return (
                <div className={style.wrapper}>
                    <WrappedComponent {...this.props} />
                    <FooterComponent page={this.props.homePage} />
                </div>
            )
        }
    }
    return connect(mapStateToProps)(PageComponent);
}