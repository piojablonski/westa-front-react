import React from 'react';
import {firstLevelNavigationHierarchy, contactInfoSelector} from "../../selectors/umbraco-node.selectors";
import {connect} from "react-redux";
import {HorizontalMenuComponent} from './horizontal-menu';
import {bindActionCreators} from "redux";
import {menuActions} from "../../reducers/menu.actions";
import {MenuDrawerComponent} from './menu-drawer';
import {MenuBackdropComponent} from './menu-backdrop';

const mapStateToProps = state => ({
    firstLevelNavigationHierarchy: firstLevelNavigationHierarchy(state),
    isMenuOpened: state.menu.isOpened,
    contactInfo : contactInfoSelector(state)
});

const mapDispatchToProps = dispatch => ({
    menuActions: bindActionCreators(menuActions, dispatch)
})

@connect(mapStateToProps, mapDispatchToProps)
export class MainMenu extends React.Component {

    constructor(props) {
        super(props);

        this.toggleMenu = () => {
            this.props.isMenuOpened ? this.props.menuActions.close() : this.props.menuActions.open();
        }
    }

    render() {
        const {isMenuOpened, firstLevelNavigationHierarchy, contactInfo} = this.props;
        return (
            <div>
                <MenuBackdropComponent
                    isMenuOpened={isMenuOpened}
                    onOverlayClick={this.toggleMenu}
                />
                <MenuDrawerComponent
                    items={firstLevelNavigationHierarchy}
                    email={contactInfo.contactEmail}
                    phone={contactInfo.contactPhone}
                    isMenuOpened={isMenuOpened}
                    afterLinkClick={this.toggleMenu}
                />
                <HorizontalMenuComponent
                    isMenuOpened={isMenuOpened}
                    children={firstLevelNavigationHierarchy}
                    onMenuButtonClick={this.toggleMenu}
                />

            </div>
        )
    }

}

