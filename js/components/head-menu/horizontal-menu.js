import React from 'react';
import {Link} from 'react-router';
import R from 'ramda';
import style from './horizontal-menu.scss';
import classNames from 'classnames';


export const HorizontalMenuComponent = (props) => {
    const createChild = child => {
        return (<Link className={classNames(style.link_item, 'flex-none')} activeClassName={style.link_active} key={child.id}
                      to={child.url}>{child.name}</Link>)
    };
    return (
        <div className={style.wrapper}>
            <nav
                className={classNames('layout-row', 'layout-align-end-center', 'layout-align-gt-md-space-between-center', style.bar)}>
                <Link to="/" className={style.logo}>
                    <svg>
                        <use xlinkHref="/assets/icons/westa-logo.svg#Layer_1"></use>
                    </svg>
                </Link>
                <div className={classNames(style.link_container, 'flex-none hide', 'show-gt-md')}>
                    {R.map(createChild, props.children)}
                </div>
            </nav>
            <button className={classNames(style.button_menu, 'hide-gt-md', 'mt', 'hamburger', 'hamburger--squeeze', {'is-active' : props.isMenuOpened})}
                    type="button" onClick={props.onMenuButtonClick}>
                <span className="hamburger-box">
                    <span className="hamburger-inner"></span>
            </span>
            </button>
        </div>
    )
};

HorizontalMenuComponent.propTypes = {
    children : React.PropTypes.array.isRequired,
    isMenuOpened : React.PropTypes.bool.isRequired,
    onMenuButtonClick : React.PropTypes.func.isRequired
}
