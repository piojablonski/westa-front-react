import React from 'react';
import classNames from 'classnames';
import style from './menu-drawer.scss';
import {Link} from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


export const MenuDrawerComponent = (props) => {

    const {items, phone, email, isMenuOpened, afterLinkClick} = props;

    const linkElem = item => (
        <Link onClick={afterLinkClick} className={classNames(style.link_item, 'flex-none')} key={item.id} to={item.url}
              activeClassName={style.link_active}>{item.name}</Link>
    );

    const menuElem = isMenuOpened ? (
            <nav className={classNames('layout-column layout-align-start-start', style.drawer)}>
                <div
                    className={classNames(style.link_list, 'layout-column layout-align-start-stretch flex-none')}>
                    {items.map(linkElem)}
                </div>
                ﻿
                <div className={classNames('flex-none layout-column', style.contact_block)}>
                    <span className={style.info_email}>email: {email}</span>
                    <span>tel: {phone}</span>
                </div>
            </nav>
        ) : null;

    return (
        <ReactCSSTransitionGroup
            transitionName={{
                enter: style.enter,
                enterActive: style.enter_active,
                leave: style.leave,
                leaveActive: style.leave_active
            }}

            transitionEnter={true}
            transitionEnterTimeout={410}
            transitionLeave={true}
            transitionLeaveTimeout={110}>
            {menuElem}
        </ReactCSSTransitionGroup>
    )


}

MenuDrawerComponent.propTypes = {
    items: React.PropTypes.array.isRequired,
    email: React.PropTypes.string.isRequired,
    phone: React.PropTypes.string.isRequired,
    isMenuOpened: React.PropTypes.bool.isRequired,
    afterLinkClick: React.PropTypes.func.isRequired

}
