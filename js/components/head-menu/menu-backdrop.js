import React from 'react';
import style from './menu-backdrop.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'


export const MenuBackdropComponent = (props) => {
    let backdrop = props.isMenuOpened ? ( <div onClick={props.onOverlayClick} className={style.wrapper}></div>) : null;
    //return null;
    return <ReactCSSTransitionGroup
        transitionName={{
            enter: style.enter,
            enterActive: style.enter_active,
            leave: style.leave,
            leaveActive: style.leave_active,
        }}

        transitionEnter={true}
        transitionEnterTimeout={410}
        transitionLeave={true}
        transitionLeaveTimeout={110}>
        {backdrop}

    </ReactCSSTransitionGroup>
}

MenuBackdropComponent.propTypes = {
    isMenuOpened: React.PropTypes.bool.isRequired,
    onOverlayClick: React.PropTypes.func.isRequired
}
