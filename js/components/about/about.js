import React from 'react';
import {Page} from "../page/page";
import {UmbracoImage} from '../shared/umbraco-image';
import classNames from 'classnames';
import styles from './about.scss';

@Page('oNas')
export class AboutComponent extends React.Component {
    render() {
        const {page} = this.props;
        return <div className={classNames(styles.about_container)}>
            <UmbracoImage className={classNames('bcg-image', styles.about_image, styles.about_image_top)}
                          umbracoImage={page.obrazekGorny} />
            <div className={classNames(styles.about_text_group)}>
                <div className={classNames(styles.about_text_container, styles.about_text_container_small)}>
                    <div className={classNames(styles.about_text_header)}>
                        <span className={styles.about_line}></span>
                        {page.tekst1Naglowek}
                    </div>
                    <p className={classNames(styles.about_text_paragraph)}>{page.tekst1Tresc}</p>
                </div>
                <div className={classNames(styles.about_text_container, styles.about_text_container_small)}>
                    <div className={classNames(styles.about_text_header)}>
                        <span className={classNames(styles.about_line)}></span>
                        {page.tekst2Naglowek}
                    </div>
                    <p className={classNames(styles.about_text_paragraph)}>
                        {page.tekst2Tresc}
                    </p>
                </div>
            </div>
            <UmbracoImage className={classNames('bcg-image', styles.about_image, styles.about_image_middle)}
                          umbracoImage={page.obrazekSrodkowy} />
            <div className={classNames(styles.about_text_group)}>
                <div className={classNames(styles.about_text_container, styles.about_text_container_small)}>
                    <div className={classNames(styles.about_text_header)}>
                        <span className={classNames(styles.about_line)}></span>
                        {page.tekst3Naglowek}
                    </div>
                    <p className={classNames(styles.about_text_paragraph)}>{page.tekst3Tresc}</p>
                </div>
                <div className={classNames(styles.about_text_container, styles.about_text_container_small)}>
                    <div className={classNames(styles.about_text_header)}>
                        <span className={classNames(styles.about_line)}></span>
                        {page.tekst4Naglowek}
                    </div>
                    <p className={classNames(styles.about_text_paragraph)}>{page.tekst4Tresc}</p>
                </div>
            </div>
            <div className={classNames(styles.about_text_group, styles.about_text_group_full)}>
                <div className={classNames(styles.about_text_container)}>
                    <div className={classNames(styles.about_text_header)}>
                        <span className={classNames(styles.about_line)}></span>
                        {page.tekst5Naglowek}
                    </div>
                    <p className={classNames(styles.about_text_paragraph)}>{page.tekst5Tresc}</p>
                </div>
            </div>
        </div>
    }
}
