import React from 'react';
import {Page} from "../page/page";
import styles from './offer-article.scss';
import classNames from 'classnames';
import {UmbracoImage} from '../shared/umbraco-image';
import {Link} from 'react-router';
import * as stringUtils from "../../stringUtils";
import Swiper from "react-id-swiper";


const MirrorLink = ({content}) =>
    content.mirrorUrl ? <Link className={styles.prj_header_button_lustrzane_odbicie} to={content.mirrorUrl}>
            <span>odbicie lustrzane</span>
            <svg>
                <use xlinkHref="/assets/icons/westa-icons.svg#odbicie-lustrzane"></use>
            </svg>
        </Link> : null;


const Room = ({room}) =>
    <div className={classNames(styles.room_info_list)}>
        <span className={classNames(styles.rooms_info_title)}>{room.title}</span>
        <span className={classNames(styles.rooms_info_data)}>{stringUtils.surfaceFormat(room.powierzchnia)}
            m<sup>2</sup></span>
    </div>;

const Details = ({content}) =>
    <div className={classNames(styles.projekt_details_block)}>
        <div className={classNames(styles.projekt_description)}>
            <div className={classNames(styles.project_field_name)}>{content.title}</div>
            <div className={classNames(styles.project_field_divider)}></div>
            <div className={classNames(styles.project_field, styles.project_field_with_margin)}>
                <span className={classNames(styles.project_field_title)}>POWIERZCHNIA UŻYTKOWA:</span>&nbsp;<span
                className={classNames(styles.project_field_data)}>{stringUtils.surfaceFormat(content.powierzchnia)}
                m<sup>2</sup></span>
            </div>
            <div className={classNames(styles.project_field)}>
                <span className={classNames(styles.project_field_title)}>CENA DOMU:</span>&nbsp;<span
                className={classNames(styles.project_field_data)}>{stringUtils.currencyZl(content.cenaDomu)}</span>
            </div>
            <div className={classNames(styles.project_field_description)}>{content.opis}</div>

        </div>
        <div className={classNames(styles.rooms_info)}>
            <span className={classNames(styles.rooms_info_header)}>Pomieszczenia</span>
            <div className={classNames(styles.rooms_info_accent_line, 'accent_line')}></div>
            {content.pomieszczenia.map((room, idx) => <Room key={idx} room={room}/>)}
        </div>
    </div>;


export const DocumentLinks = ({content}) =>
    <div className={classNames(styles.document_links)}>
        {
            content.zakresPrac && (
                <Link to={stringUtils.backendUrl(content.zakresPrac.url)} target="_blank"
                      className={classNames(styles.document_links_button, styles.document_links_button_zakres_prac)}>
                    <svg>
                        <use xlinkHref="/assets/icons/westa-icons.svg#zakres-prac"></use>
                    </svg>
                    <div>
                        <span>zakres prac</span>
                    </div>
                </Link>
            )
        }
        {
            content.kartaProjektu && (
                <Link to={stringUtils.backendUrl(content.kartaProjektu.url)} target="_blank"
                      className={classNames(styles.document_links_button, styles.document_links_button_karta_projektu)}>
                    <svg>
                        <use xlinkHref="/assets/icons/westa-icons.svg#karta-projektu"></use>
                    </svg>
                    <div>
                        <span>karta projektu</span>
                    </div>
                </Link>
            )
        }
    </div>;


const FloorPlan = ({plan, content}) =>
    plan ? <div className={styles.plan}>
            <img src={stringUtils.backendUrl(plan.url)} className={styles.plan_image}/>
            <div className={styles.plan_description}>
                <span className={styles.plan_description_header}>{content.title}</span>
            </div>
        </div> : null;


export const Visualisations = ({content}) =>
    <div className={styles.visualisations}>
        {content.wizualizacje.map((w, idx) =>
            <img key={idx} className={styles.visualisations_image} src={stringUtils.backendUrl(w.url)}/>
        )}
    </div>


export const SwiperVisualisations = ({content}) =>
    content.wizualizacje ?
        (
            <div className={styles.swiper_visualisations}>
                <Swiper
                    pagination='.swiper-pagination'
                    nextButton='.swiper-button-next'
                    prevButton='.swiper-button-prev'
                    preloadImages={false}
                    lazyLoading={true}
                    loop={true}
                    centeredSlides={true}
                >
                    {content.wizualizacje.map((w, idx) =>
                        <div key={idx}>
                            <img className={styles.swiper_visualisations_image} src={stringUtils.backendUrl(w.url)}/>
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </div>
                    )}
                </Swiper>
            </div>
        ) : null;


@Page()
export class OfferArticleComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {page, homePage} = this.props;
        return (
            <div className={classNames(styles.projekt_container)}>
                <div className={classNames(styles.prj_header)}>
                    <UmbracoImage className={classNames('bcg-image', styles.prj_header_image)}
                                  umbracoImage={page.naglowekSzeroki}/>
                    <MirrorLink content={page}/>


                </div>
                <Details content={page}/>
                <DocumentLinks content={page}/>
                <FloorPlan content={page} plan={page.planParteru}/>
                <FloorPlan content={page} plan={page.planPietra}/>
                <SwiperVisualisations content={page}/>

            </div>
        );
    }
}