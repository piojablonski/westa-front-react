import React from 'react';
import classNames from 'classnames';
import style from './home.component.scss';
import {connect} from "react-redux";
import {pageDataSelector} from "../../selectors/umbraco-node.selectors";
import {appConfig} from "../../appConfig";
import {Link} from 'react-router';
import {UmbracoImage} from '../shared/umbraco-image';
import {FooterComponent} from '../';
import {Page} from "../page/page";

const TopDividerComponent = props => {
    return (
        <div className={style.top_divider_container}>
            <span className={classNames(style.top_divider_arrow, style.top_divider_arrow_left)}></span>
            <div className={style.top_divider_logo}>
                <svg>
                    <use xlinkHref="/assets/icons/westa-logo.svg#Layer_1"></use>
                </svg>
            </div>
            <span className={classNames(style.top_divider_arrow, style.top_divider_arrow_right)}></span>
        </div>

    )
}

const ArticlesComponent = props => {
    const {page} = props;
    return (
        <div className={classNames(style.articles_container)}>
            <div key="1" className={classNames(style.articles_article)} to='oferta-domow'>
                <span className={classNames(style.articles_header)}>oferta domów</span>
                <UmbracoImage className={classNames('bcg-image', style.articles_image)}
                              umbracoImage={page.artykul1Zdjecie}/>
                <div className={classNames(style.articles_background)}>
                    <p className={classNames(style.articles_intro)}>{page.artykul1Naglowek}</p>
                    <p className={classNames(style.articles_content)}>{page.artykul1Tresc}</p>
                    <Link className={classNames(style.articles_link)} to='oferta-domow'>zobacz OFERTĘ DOMÓW</Link>
                </div>
            </div>
            <div key="2" className={classNames(style.articles_article)} to='domy-indywidualne'>
                <span className={classNames(style.articles_header)}>domy indywidualne</span>
                <UmbracoImage className={classNames('bcg-image', style.articles_image)}
                              umbracoImage={page.artykul2Zdjecie}/>
                <div className={classNames(style.articles_background)}>
                    <p className={classNames(style.articles_intro)}>{page.artykul2Naglowek}</p>
                    <p className={classNames(style.articles_content)}>{page.artykul2Tresc}</p>
                    <Link className={classNames(style.articles_link)} to='domy-indywidualne'>zobacz DOMY
                        INDYWIDUALNE</Link>
                </div>
            </div>
        </div>
    )


}
ArticlesComponent.propTypes = {
    page: React.PropTypes.object.isRequired
}

const BottomDividerComponent = props => {

    return (
        <div className={classNames(style.bottom_divider_container)}>
            <span className={classNames(style.bottom_divider_arrow, style.bottom_divider_arrow_left)}></span>
            <Link to="realizacje" className={classNames(style.bottom_divider_link)}>zobacz nasze realizacje</Link>
            <span className={classNames(style.bottom_divider_arrow, style.bottom_divider_arrow_right)}></span>
        </div>
    )
}

const BottomDescription = props => {
    const {page} = props;
    return (
        <div className={classNames(style.westa_description_container)}>
            <p className={classNames(style.westa_description_article)}>
                {page.trescOpisu}
            </p>
        </div>
    )
}
BottomDescription.propTypes = {
    page: React.PropTypes.object.isRequired
}

@Page('homePage')
export class HomeComponent extends React.Component {
    render() {
        const {page} = this.props;
        return (
            <div className={classNames(style.container, style.page_content_container)}>
                <UmbracoImage className={classNames('bcg-image', style.header_image)}
                              umbracoImage={page.zdjecieNaglowek}></UmbracoImage>
                <TopDividerComponent/>
                <ArticlesComponent page={page}/>
                <BottomDividerComponent/>
                <BottomDescription page={page}/>
            </div>
        );
    }
}







