import React from 'react';
import {Link} from 'react-router';
import style from '../offer-list/offer-list.component.scss';
import classNames from 'classnames';
import R from 'ramda';
import {Page} from "../page/page";
import * as stringUtils from '../../stringUtils';

@Page('listaRealizacji')
export class RealizationListComponent extends React.Component {
    render() {
        const {page} = this.props;
        return (
            <div className={style.container}>
                <div className={classNames(style.projects_list)}>
                    {R.addIndex(R.map)((prj, idx) => <Project projekt={prj} key={idx}/>, page.children)}
                </div>
            </div>

        );
    }
}

const Project = ({projekt}) =>
    <Link to={projekt.url} className={classNames(style.projects_list_item)}>
        <img className={classNames(style.projects_list_image)}
             src={stringUtils.backendUrl(projekt.naglowek.url)}/>
        <div style={{justifyContent: 'space-between'}} className={classNames(style.projects_list_info_block)}>
            <span className={classNames(style.projects_list_triangle)}></span>
            <div style={{maxWidth:'100%'}} className={classNames(style.projects_list_info_name)}>
                <span className={classNames(style.projects_list_accent_line, 'accent-line')}></span>
                <span className={classNames(style.projects_list_label_name)}>{projekt.title}</span>
            </div>
            <div style={{flexGrow:0}} className={classNames(style.projects_list_info_surface)}>
                <span className={classNames(style.projects_list_label)}>POWIERZCHNIA UŻYTKOWA:</span>
                <span
                    className={classNames(style.projects_list_value)}>{stringUtils.surfaceFormat(projekt.powierzchnia)}
                    m<sup>2</sup></span></div>

        </div>
    </Link>
