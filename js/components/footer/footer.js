import React from 'react';
import classNames from 'classnames';
import style from './footer.scss';
import {connect} from "react-redux";
import {pageDataSelector} from "../../selectors/umbraco-node.selectors";
import {appConfig} from "../../appConfig";
import {Link} from 'react-router';
import {UmbracoImage} from '../shared/umbraco-image';
import R from 'ramda';

export class FooterComponent extends React.Component {
    render() {

        const {page} = this.props;
        const mapSocialLinks = link => (
            <Link key={link.link} className={classNames('flex-none')} to={link.link} target="_blank">{link.caption}</Link>)

        return (
            <footer className={style.wrapper}>
                <div className={classNames(style.body_footer)}>
                    <div className={classNames(style.footer_item)}>
                        <h2 className={classNames(style.footer_header)}>Dane firmy</h2>
                        <p className={classNames(style.footer_paragraph)}>{page.daneFirmy}</p>
                    </div>
                    <div className={classNames(style.footer_item)}>
                        <h2 className={classNames(style.footer_header)}>Kontakt</h2>
                        <div className={classNames(style.footer_paragraph, 'layout-column')}>
                            <span className={classNames('flex-none')}>email: {page.contactEmail}</span>
                            <span className={classNames('flex-none')}>tel: {page.contactPhone}</span>
                        </div>
                    </div>
                    <div className={classNames(style.footer_item)}>
                        <h2 className={classNames(style.footer_header)}>Socialmedia</h2>
                        <div className={classNames(style.footer_paragraph, 'layout-column')}>
                            {R.map(mapSocialLinks, page.socialLinks)}
                        </div>
                    </div>

                </div>
            </footer>
        )
    }
}