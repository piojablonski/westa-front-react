import React from 'react';
import {Page} from "../page/page";
import {UmbracoImage} from '../shared/umbraco-image';
import {GridRenderer} from '../shared/grid-renderer';
import classNames from 'classnames';
import styles from './individual-houses.scss';


@Page('domyIndywidualne')
export class IndividualHousesComponent extends React.Component {

    render() {

        const {page} = this.props;
        return (
            <div className={styles.container}>
                <UmbracoImage className={classNames('bcg-image', styles.ws_image)}
                              umbracoImage={page.obrazekNaglowek}></UmbracoImage>
                <div className={styles.ws_step_2_container}>
                    <span className={classNames('accent-line', styles.ws_accent_line)}></span>
                    <h2 className={styles.ws_header}>{page.title}</h2>
                    <h3 className={styles.ws_subheader}>oferta dostosowana do Ciebie</h3>
                    <div className={styles.ws_description}>
                        <GridRenderer grid={page.blog}/>
                    </div>
                </div>
            </div>

        );
    }
}
