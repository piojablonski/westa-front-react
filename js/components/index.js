export {HomeComponent} from './home/home.component';
export {NotFoundComponent} from './not-found/not-found.component';
export {HorizontalMenuComponent, MainMenu} from './head-menu/head-menu';
export {AboutComponent} from './about/about';
export {ContactComponent} from './contact/contact';
export {IndividualHousesComponent} from './individual-houses/individual-houses';
export {OfferListComponent} from './offer-list/offer-list.component';
export {OfferArticleComponent} from './offer-article/offer-article';
export {RealizationArticleComponent} from './realization-article/realization-article';
export {RealizationListComponent} from './realization-list/realization-list';
export {ScopeOfWorkComponent} from './work-scope/scope-work';
export {FooterComponent} from './footer/footer';

