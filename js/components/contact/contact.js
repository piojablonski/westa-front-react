import React from 'react';
import {Page} from "../page/page";
import {UmbracoImage} from '../shared/umbraco-image';
import {GridRenderer} from '../shared/grid-renderer';
import classNames from 'classnames';
import styles from './contact.scss';
import {AppLoadingIndicator} from '../shared/app-loading-indicator';
import {reduxForm, Form, Field, startSubmit, stopSubmit} from 'redux-form';
import R from 'ramda';
import * as validatejs from 'validate.js';
import {connect} from "react-redux";
import {sendContactForm} from "../../reducers/contact-form.actions";

export const ContactHeader = ({page, homePage}) =>
    <div className={classNames(styles.contact_info_container)}>
        <div className={classNames(styles.company_info_containe)}>
            <h2 className={classNames(styles.contact_header, styles.company_info_header)}>kontakt</h2>
            <span
                className={classNames(styles.contact_text, styles.company_info_email)}>email: {homePage.contactEmail}</span>
            <span
                className={classNames(styles.contact_text, styles.company_info_phone)}>tel: {homePage.contactPhone}</span>
            <p className={classNames(styles.contact_text, styles.company_info_summary)}>{homePage.daneFirmy}</p>
        </div>
        <div className={classNames(styles.perimeter_info_container)}>
            <h2 className={classNames(styles.contact_header, styles.perimeter_info_header)}>zasięg działania</h2>
            <p className={classNames(styles.contact_text, styles.perimeter_info_summary)}>{page.zasiegDzialania}</p>
        </div>
    </div>;

@Page('kontakt')
export class ContactComponent extends React.Component {

    render() {
        const {page, homePage} = this.props;
        return (
            <div className={classNames(styles.contact_container)}>
                <ContactHeader page={page} homePage={homePage}/>
                <ContactForm  />
            </div>

        )
    }

}

const validate = values => {

    const isNilOrEmpty = R.or(R.isNil, R.isEmpty);
    let errors = {};
    if (isNilOrEmpty(values.email) && isNilOrEmpty(values.phone)) {
        errors.email = ["Proszę podać numer telefonu lub adres email."]
        errors.phone = "Proszę podać numer telefonu lub adres email."
    } else {

        var constraints = {
            email: {
                email: {
                    message: 'podaj poprawny email'
                }
            }
        };

        errors = validatejs.validate({...values}, constraints);
    }
    return errors;
}


const renderField = props => {
    const {input, meta : {touched, error}, placeholder, type} = props;
    return (
        <span className={styles.contact_form_field_container}>
            <input {...input} className={classNames(styles.contact_form_text_input)} placeholder={placeholder}
                   type={type}/>
            {touched && <span className={classNames(styles.field_validation_error)}>{error}</span>}
        </span>
    )
}

@connect()
@reduxForm({
    form: 'contact',
    validate
})
export class ContactForm extends React.Component {

    constructor(props) {
        super(props);

        console.log('ContactForm constructor', props);
        this.send = this.send.bind(this);
        this.sendSuccess = this.sendSuccess.bind(this);
        this.sendFailed = this.sendFailed.bind(this);


    }

    send(formValues) {
        console.log('sending form', formValues);
        let p = sendContactForm(formValues);

        const {dispatch} = this.props;
        dispatch(startSubmit('contact'));
        return dispatch(sendContactForm(formValues))
            .then(res=>{
                this.sendSuccess();
                alert("wiadomość została wysłana");
               // return Promise.resolve();
            })
            .catch(reason=>{
                this.sendFailed();
              //  return Promise.reject();
            })

    }

    sendSuccess() {
        const {dispatch} = this.props;
        console.log('success')
        dispatch(stopSubmit('contact'));
    }

    sendFailed() {
        const {dispatch} = this.props;
        console.log('failed')
        dispatch(stopSubmit('contact'));
    }


    render() {
        const {handleSubmit, submitting, submitSucceeded, submitFailed} = this.props;
        console.log({submitting, submitSucceeded, submitFailed});
        return (
            <Form onSubmit={handleSubmit(this.send)} className={classNames(styles.contact_form_container)}>
                <h2 className={classNames(styles.contact_form_header)}>formularz kontaktowy</h2>
                <Field component={renderField} placeholder="imię i nazwisko" type="text" name="name"/>
                <Field component={renderField} placeholder="*adres email" type="text" name="email"/>
                <Field component={renderField} placeholder="*telefon" type="text" name="phone"/>
                <Field component="textarea"
                       className={classNames(styles.contact_form_text_input, styles.contact_form_textarea_input)}
                       cols="20" name="message" placeholder="treść wiadomości" rows="2"/>
                <button type="submit" className={classNames(styles.contact_form_send_button)}
                        disabled={submitting}>wyślij
                </button>
                {submitting && <div className={classNames(styles.contact_form_loading_overlay)}>
                    <AppLoadingIndicator/>
                </div>}

            </Form>
        )
    }
}





