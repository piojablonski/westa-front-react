import {CMS_GET_DATA} from "./cms.actions";
export const cmsReducer = (state = {}, action) => {

    switch(action.type) {
        case CMS_GET_DATA:
            return action.data;
        default:
            return state;
    }
}