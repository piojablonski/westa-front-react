import {startSubmit, stopSubmit} from "redux-form";
import * as axios from "axios";
import {appConfig} from "../appConfig";

var baseContactApi = `${appConfig.backendUrl}umbraco/api/contactapi/`;


export const CMS_SEND_CONTACT_FORM = 'CMS_SEND_CONTACT_FORM';


export const sendContactForm = formValues => {
    return dispatch => {
        return axios.post(`${baseContactApi}send`, formValues);
    }
}
