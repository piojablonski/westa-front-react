import axios from 'axios';
import {appConfig} from "../appConfig";

export const CMS_GET_DATA = 'CMS_GET_DATA';


var baseContentApi = `${appConfig.backendUrl}umbraco/api/contentapi/`

const getData = () => {
    return dispatch => {
        axios.get(`${baseContentApi}nodebyurl?url=/`)
            .then(res=>{
                dispatch({type: CMS_GET_DATA, data: res.data});
            });
    }


}



export const cmsActions = { getData }



