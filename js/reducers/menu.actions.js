const MENU_OPEN = 'MENU_OPEN';
const MENU_CLOSE = 'MENU_CLOSE';

export const menuActionNames = {MENU_OPEN, MENU_CLOSE};

function open() {
    return { type: MENU_OPEN };
}

function close() {
    return { type: MENU_CLOSE };
}

export const menuActions = {open, close};
