import {menuActionNames} from "./menu.actions";
export const menuReducer = (state = {}, action) => {
    switch(action.type) {
        case menuActionNames.MENU_OPEN :
            return {
                isOpened : true
            };
        case menuActionNames.MENU_CLOSE :
            return {
                isOpened : false
            }
        default :
            return state;
    };
};
