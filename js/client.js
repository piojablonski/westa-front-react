import React from "react";
import ReactDOM from 'react-dom';
import {Router, Route, Link, browserHistory, IndexRoute, hashHistory} from 'react-router';
import {combineReducers, createStore, applyMiddleware, bindActionCreators} from 'redux';
import {Provider, connect} from 'react-redux';
import {routerReducer, syncHistoryWithStore} from 'react-router-redux';
import {composeWithDevTools, devToolsEnhancer} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {cmsReducer} from "./reducers/cms.reducer";
import {initialDebugState} from "./initialState";
import './global.scss';
import {menuReducer} from "./reducers/menu.reducer";
import * as Comp from "./components";
import {cmsActions} from "./reducers/cms.actions"
import {reducer as formReducer} from 'redux-form';

import './index.html';


@connect(
    state => ({cms: state.cms}),
    // dispatch => ({
    //     cmsActions: bindActionCreators(cmsActions, dispatch)
    // })
)
class MainLayout extends React.Component {

    componentDidMount() {
       // this.props.dispatch(cmsActions.getData());
    }

    render() {
        if (!this.props.cms)
            return null;
        else
            return (
                <div className="layout-column flex">
                    <Comp.MainMenu />
                    {this.props.children}
                </div>
            );
    }
}

class AppRouter extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Route path="/" component={MainLayout}>
                    <IndexRoute component={Comp.HomeComponent}/>
                    <Route path="oferta-domow">
                        <IndexRoute component={Comp.OfferListComponent}/>
                        <Route path=":id" component={Comp.OfferArticleComponent}/>
                    </Route>
                    <Route path="domy-indywidualne" component={Comp.IndividualHousesComponent}/>
                    <Route path="realizacje">
                        <IndexRoute component={Comp.RealizationListComponent}/>
                        <Route path=":id" component={Comp.RealizationArticleComponent}/>
                    </Route>
                    <Route path="zakres-prac" component={Comp.ScopeOfWorkComponent}/>
                    <Route path="o-nas" component={Comp.AboutComponent}/>
                    <Route path="kontakt" component={Comp.ContactComponent}/>
                </Route>
                <Route path="*" component={Comp.NotFoundComponent}/>
            </Router>
        );
    }
}

const store = createStore(
    combineReducers({
        cms: cmsReducer,
        routing: routerReducer,
        menu: menuReducer,
        form : formReducer
    }),
    initialDebugState,
    composeWithDevTools(applyMiddleware(thunk))
);
const history = syncHistoryWithStore(hashHistory, store)

ReactDOM.render(
    <Provider store={store}>
        <AppRouter/>
    </Provider>,
    document.getElementById("root"));


