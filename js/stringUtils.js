import {appConfig} from "./appConfig";
import numeral from 'numeral';

export function imageQuality(value) {
    return `${value}?quality=80`;
}

export function currencyZl(value) {
    if (!value) {
        console.error('currenyZl, value not found');
        return '';
    }
    let val = value.toFixed(2).replace(/./g, function(c, i, a) {
        return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c;
    });

    return `${val} zł`;
}

export function backendUrl(value) {
    return `${appConfig.backendUrl}${value}`;
}

export function backendImageUrl(value, props = {quality : 80} ) {
    return `${appConfig.backendUrl}${value}?${toQueryString(props)}`;
}

export function surfaceFormat(value) {
    return numeral(value).format('0.00');
}

function toQueryString(paramsObject) {
    return Object
        .keys(paramsObject)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(paramsObject[key])}`)
        .join('&')
        ;
}

