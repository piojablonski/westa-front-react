import {createSelector} from 'reselect';
import R from 'ramda';

export const firstLevelNavigationHierarchy = createSelector(
    state => state.cms.children,
    (children) => {
        let extractData = child => {
            let {url, meta : {id, name}} = child;
            return {id, url, name};

        };
        let result = R.map(extractData, children);
        return result;
    }
);

function findUmbNode(rootNode, predicate) {
    if (predicate(rootNode)) {
        return rootNode;
    } else if (rootNode.children.length > 0) {
        for (let i = 0; i < rootNode.children.length; i++) {
            let res = findUmbNode(rootNode.children[i], predicate);
            if (res != null) {
                return res;
            }
        }
    } else {
        return null;
    }
}

export const contactInfoSelector = createSelector(
    state => state.cms,
    cms => {
        const node = findUmbNode(cms, node => node.meta.documentTypeAlias == 'homePage');
        const {contactPhone, contactEmail} = node;
        return node;
    }
);

const getCurrentUrl = state => state.routing.locationBeforeTransitions.pathname;

export const getUmbracoNodeByTypeAlias = (state, documentTypeAlias) => {
    if (!documentTypeAlias)
        return null;
    const umbNode = findUmbNode(state.cms, node => node.meta.documentTypeAlias == documentTypeAlias);
    if (!umbNode) {
        console.error(`umbraco node ${documentTypeAlias} not found`);
        return null;
    }
    return umbNode;
}

export const getUmbracoNodeByUrlName = (state, urlName) => {
    const umbNode = findUmbNode(state.cms, node => node.meta.urlName == urlName);
    if (!umbNode)
        console.error(`umbraco node ${urlName} not found`);
    return umbNode;
}

export const getUmbracoNodeByUrl = createSelector(
    [getUmbracoNodeByTypeAlias, state => state, getCurrentUrl], (umbNode, state, url) => {
        if (umbNode)
            return umbNode;
        const offerListReg = /\/oferta-domow\/$/;
        const offerDetailsReg = /\/oferta-domow\/(.*)\/$/;
        const realizationDetailsReg = /\/realizacje\/(.*)\/$/;

        let result = null;

        switch (true) {
            case offerListReg.test(url) :
                result = getUmbracoNodeByTypeAlias(state, 'listaProjektow');
                return {
                    ...result,
                    children: R.filter(c => c.meta.documentTypeAlias == 'projekt')(result.children)
                }
                break;
            case offerDetailsReg.test(url):
                let urlName = offerDetailsReg.exec(url)[1];
                let mainProject,
                    variantProject,
                    combinedProject,
                    mirrorUrl,
                    currentProject = getUmbracoNodeByUrlName(state, urlName);

                if (currentProject.meta.documentTypeAlias == "projekt") {
                    mainProject = currentProject;
                    variantProject = findUmbNode(state.cms, prj => prj.projektGlowny == currentProject.meta.id);
                    combinedProject = mainProject;

                    if (variantProject) {
                        mirrorUrl = variantProject.meta.url;
                    }
                }
                else {
                    mainProject = findUmbNode(state.cms, prj => prj.meta.id == currentProject.projektGlowny);
                    variantProject = currentProject;

                    combinedProject = R.mergeWith((mprop, vprop) => R.isNil(vprop) ? mprop : vprop)(mainProject, variantProject);
                    mirrorUrl = mainProject.meta.url;
                }

                let homePage = state.cms;
                let zakresPrac = (combinedProject.iloscKondygnacji == "jednopoziomowy") ? homePage.zakresPracProjektyParterowe : homePage.zakresPracProjektyPietrowe;

                return {
                    ...combinedProject,
                    zakresPrac,
                    mirrorUrl
                };

            case realizationDetailsReg.test(url) :
                let urlName2 = realizationDetailsReg.exec(url)[1];
                return getUmbracoNodeByUrlName(state, urlName2);


            default:
                console.log('node not found for', url);

        }
        return result;
    });


export const pageDataSelector = createSelector(
    getUmbracoNodeByUrl,
    (umbNode, a, b) => {
        const {...n} = umbNode;
        return n;
    }
)
